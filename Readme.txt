Project developed by Alberto Gutierrez.

Operand overloaded
=
+
+=
==
!=
<<
>>

For char *, string and MString

Is enable the use of perfect forwarding MSTring(&&MString).

Feel free to contribute to this Smart string adding new functions.


Coming features:
 * Add a method translate(from, to). Thinking in queries to bing or google translator. 
 * Use of threads for big strings.