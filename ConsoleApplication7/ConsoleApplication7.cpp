// ConsoleApplication7.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#include <iostream>


using namespace std;


namespace std {

  class MString
  {
  private:
    unsigned int size;
    char *buffer;

  public:
    MString() : size(0), buffer(nullptr){}

    MString(const MString& other) {
      this->size = other.size;
      this->buffer = new char[this->size + 1];
      memcpy(this->buffer, other.buffer, this->size);
      this->buffer[this->size] = 0;
    }

    MString(const char * other) {
      this->size = strlen(other);
      this->buffer = new char[this->size + 1];
      memcpy(this->buffer, other, this->size);
      this->buffer[this->size] = 0;
    }

    MString(char  other) {
      this->size = 1;
      this->buffer = new char[this->size + 1];
      memcpy(this->buffer, &other, this->size);
      this->buffer[this->size] = 0;
    }

    MString(string& other) {
      this->size = other.size();
      this->buffer = new char[this->size + 1];
      memcpy(this->buffer, other.c_str(), this->size);
      this->buffer[this->size] = 0;
    }

    MString(MString&& other) {
      this->size = other.size;
      this->buffer = nullptr;
      std::swap(this->buffer, other.buffer);
    }

    ~MString() {
      if (this->buffer != nullptr)
        delete[]this->buffer;
    }


    MString& operator=(const MString& s1){
      this->size = s1.size;
      this->buffer = new char[this->size + 1];
      memcpy(this->buffer, s1.buffer, this->size);
      return *this;
    }

    MString& operator=(MString&& s1){
      this->size = s1.size;
      std::swap(this->buffer, s1.buffer);
      return *this;
    }

    bool operator==(const MString& s1) const {
      if (this->size != s1.size) return false;
      return (strcmp(this->buffer, s1.buffer)) ? false : true;
    }

    bool operator!=(const MString& s1) const {
      if (this->size != s1.size) return true;
      return (strcmp(this->buffer, s1.buffer)) ? true : false;
    }
    

    const char * c_str() const{
      return this->buffer;
    }

    void revert(){
      unsigned int end = this->size - 1;
      for (unsigned int i = 0; i < this->size; ++i){
        if ((i == end) || (i > end)) break;
        std::swap(this->buffer[i], this->buffer[end]);
        end--;
      }
    }

    void capital(){
      for (unsigned int i = 0; i < this->size; ++i){
        int ascii = static_cast<int>(this->buffer[i]);
        if ((ascii > 96) && (ascii < 123))
          this->buffer[i] = static_cast<char>(ascii - 32);
      }
    }

    void lowercase() {
      for (unsigned int i = 0; i < this->size; ++i){
        int ascii = static_cast<int>(this->buffer[i]);
        if ((ascii > 64) && (ascii < 91))
          this->buffer[i] = static_cast<char>(ascii + 32);
      }
    }
    

    friend MString operator+(const MString& s1, const MString&s2);
    friend MString operator+(const char* s1, const MString&s2);
    friend MString operator+(const MString& s1, const char *s2);
    friend ostream& operator<<(ostream& os, const MString& s1);
    friend istream& operator>>(istream& is, MString& s2);
    friend MString operator+(const MString& s1, char s2);
    friend MString operator+(char s1, const MString&s2);


  };

  MString operator+(const MString& s1, const MString&s2) {
    MString a;
    a.size = s1.size + s2.size;
    a.buffer = new char[(s1.size + s2.size) + 1];
    memcpy(a.buffer, s1.buffer, s1.size);
    memcpy(a.buffer + s1.size, s2.buffer, s2.size);
    a.buffer[a.size] = 0;
    return a;
  }

  MString operator+(const MString& s1, const char *s2) {
    return s1 + MString(s2);
  }

  MString operator+(const char* s1, const MString&s2) {
    return MString(s1) + s2;
  }

  MString operator+(const MString& s1, char s2) {
    return s1 + MString(s2);
  }

  MString operator+(char s1, const MString&s2) {
    return MString(s1) + s2;
  }

  ostream& operator<<(ostream& os, const MString& s1) {
    os << s1.buffer;
    return os;
  }


  istream& operator>>(istream& is, MString& s2){
    is >> s2.buffer;
    return is;
  }


}







int main() {


  return 0;
}

